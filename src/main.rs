use lambda_http::{run, service_fn, Body, Error, Request, Response};
use std::collections::HashMap;
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use anyhow::Result;
use std::convert::Infallible;
use std::path::PathBuf;

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let query_params = event.uri().query().unwrap_or("");
    let query_map: HashMap<String, String> = serde_urlencoded::from_str(query_params).expect("Invalid query");
    let input_res = match query_map.get("text") {
        Some(input) => input,
        None => {
            let resp = Response::builder()
            .status(500)
            .header("content-type", "text/html")
            .body(("invalid format").into())
            .map_err(Box::new)?;
            return Ok(resp);
        }
    };
    let model = llm::load_dynamic(
        Some(llm::ModelArchitecture::GptNeoX),
        &PathBuf::from("/mini10/pythia-160m-q4_0-ggjt.bin"),
        llm::TokenizerSource::Embedded,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;
    let mut session = model.start_session(Default::default());
    let mut response_text = String::new();
    let _ = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: input_res.into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(8),
        },
        &mut Default::default(),
        |response| match response {
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                response_text.push_str(&token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );
    let resp = Response::builder()
    .status(200)
    .header("content-type", "text/html")
    .body(Body::from(response_text.replace("<|padding|>", "")))
    .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
