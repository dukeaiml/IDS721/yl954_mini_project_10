# Yl954 Mini Project 10
This project is a Rust Serverless Transformer Endpoint. we will dockerize Hugging Face Rust transformer, deploy container to AWS Lambda, and implement query endpoint.

## Env Setup
1. create a new cargo lambda project: ```cargo lambda new yl954_mini_project_10```
2. dependancies
```
[dependencies]
lambda_http = "0.11.1"
tokio = { version = "1", features = ["macros"] }
tracing = { version = "0.1", features = ["log"] }
tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
serde_urlencoded = "0.7"
anyhow = "1.0.0"
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
rand = "0.8.5"
```

## Model
```pythia-160m-q4_0-ggjt.bin``` is used.

## Test Locally Using ```cargo lambda watch```
![](test.png)

## Dockerize
1. ```sudo docker build -t mini10 .```
2. ```aws ecr create-repository --repository-name mini10 --region us-east-1```
3. ```aws ecr get-login-password --region us-east-1 | sudo docker login --username AWS --password-stdin 471112888499.dkr.ecr.us-east-1.amazonaws.com```
4. ```sudo docker tag mini10:latest 471112888499.dkr.ecr.us-east-1.amazonaws.com/mini10:latest```
5. ```sudo docker push 471112888499.dkr.ecr.us-east-1.amazonaws.com/mini10:latest```
![](docker.png)

## Deploy to AWS Lambda
![](deploy.png)

## Query Endpoint Testing
```https://lwdff6j4mt43ndzqzw5vrsynou0hzgin.lambda-url.us-east-1.on.aws/?text=I%20am```
![](testremote.png)
