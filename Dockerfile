FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder
WORKDIR /usr/src/app
COPY . .
RUN cargo lambda build --release
FROM public.ecr.aws/lambda/provided:al2-x86_64
WORKDIR /mini10
COPY --from=builder /usr/src/app/target/ ./ 
COPY --from=builder /usr/src/app/src/pythia-160m-q4_0-ggjt.bin ./ 
RUN if [ -d /mini10/lambda/mini10/ ]; then echo "Directory exists"; else echo "Directory does not exist"; fi
RUN if [ -f /mini10/lambda/mini10/bootstrap ]; then echo "File exists"; else echo "File does not exist"; fi
ENTRYPOINT ["/mini10/lambda/mini10/bootstrap"]