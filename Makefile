watch:
	cargo lambda watch

build:
	cargo lambda build --release

deploy:
	cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112888499:role/for_cargo_lambda
